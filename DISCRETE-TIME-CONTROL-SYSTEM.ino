#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>

#include "Arduino.h"
// Software SPI (slower updates, more flexible pin options):
// pin 3 - Serial clock out (SCLK)
// pin 4 - Serial data out (DIN)
// pin 5 - Data/Command select (D/C)
// pin 6 - LCD chip select (CS)
// pin 7 - LCD reset (RST)
Adafruit_PCD8544 display = Adafruit_PCD8544(3, 4, 5, 6, 7);
# define LED_1_PIN 10
# define LED_2_PIN 11
# define LED_3_PIN 12
# define LED_4_PIN 13
# define LED_5_PIN 14
# define LED_6_PIN 15
# define LED_7_PIN 16
# define BUTTON_PIN1 2
# define BUTTON_PIN2 8
# define BUTTON_PIN3 9
#define clock 12
#define data 11
#define latch 10
int n = 10; //tacts
int selectdrive = 0;
int selectrepeat = 1;

int asd[] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1}; //cycle array

int asd1[] = {0, 1, 0, 0, 0, 0, 0, 0, 0, 0}; //cycle array
int asd2[] = {0, 1, 0, 0, 0, 0, 0, 0, 0, 0}; //cycle array
boolean cycle = true; //ready to start
int page = 1;
int menuitem = 1;
int i;
int xArray = 0;


boolean states[8];


class Button {
  private:
    byte _pin;
  public:

    int lastButtonState;
    boolean click;
    void checkPress() {

      if (digitalRead(_pin) != lastButtonState) {

        if (digitalRead(_pin) == 0) {
          click = true;
        }
        delay(50);
      }
      lastButtonState = digitalRead(_pin);
    }
    void setPin(byte pin) {
      _pin = pin;
      pinMode(_pin, INPUT_PULLUP);

    }

}
; Button down
; Button select
; Button back;

void setup() {
  Serial.begin(9600);
  pinMode(LED_1_PIN, OUTPUT);
  pinMode(LED_2_PIN, OUTPUT);
  pinMode(LED_3_PIN, OUTPUT);
  pinMode(LED_4_PIN, OUTPUT);
  pinMode(LED_5_PIN, OUTPUT);
  pinMode(LED_6_PIN, OUTPUT);
  pinMode(LED_7_PIN, OUTPUT);
  display.begin();
  display.setContrast(40); //Set contrast to 40
  display.clearDisplay();
  display.print("  Loading");
  delay(1000);
  display.display();

  //  pins for register
  pinMode(clock, OUTPUT);
  pinMode(data, OUTPUT);
  pinMode(latch, OUTPUT);
  digitalWrite(latch, HIGH);
  //  close register lath to get the signal from arduino
  cleanreg();
  //  fill 0 byte to prevent fail signal

  down.setPin(BUTTON_PIN1);
  select.setPin(BUTTON_PIN2);
  back.setPin(BUTTON_PIN3);

}

void loop() {
  down.checkPress();
  select.checkPress();
  back.checkPress();


  digitalWrite(latch, LOW);
  shiftOut(data, clock, LSBFIRST, 0b10000000);
  digitalWrite(latch, HIGH);
  drawMenu();//Drive select
  if (back.click && page == 1 && menuitem == 1) {
    back.click = false;
    selectdrive++;
    if (selectdrive == 2) {
      selectdrive = 0;
    }
  }
  if (select.click && page == 1 && menuitem == 1) {
    select.click = false;
    page = 2;
  }
  //углубление
  if (down.click && page == 1) {
    down.click = false;
    menuitem++;
    if (menuitem == 4) {
      menuitem = 1;
    }
  }
  // переключение ON вывод в исходную позицию
  if (select.click && back.click && menuitem == 2) {
    if (cycle == false) {
      cycle = true;
    }
    select.click = false;
    back.click = false;
    delay(500);
    digitalWrite(LED_1_PIN, LOW);
    delay(1000);
    digitalWrite(LED_2_PIN, LOW);
    delay(1000);
    digitalWrite(LED_3_PIN, LOW);
    delay(1000);
    digitalWrite(LED_4_PIN, LOW);
    delay(1000);
    digitalWrite(LED_5_PIN, LOW);
    delay(1000);
    digitalWrite(LED_6_PIN, LOW);
    delay(1000);
    digitalWrite(LED_7_PIN, LOW);
    delay(1000);

  }
  // выбор числа повторений
  if (select.click && page == 1 && menuitem == 2) {
    select.click = false;
    selectrepeat++;
    if (selectrepeat == 128) {
      selectrepeat = 1;
    }
  }
  if (back.click && page == 1 && menuitem == 2) {
    back.click = false;
    selectrepeat--;
    if (selectrepeat == 0) {
      selectrepeat = 127;
    }
  }

  if (select.click && back.click && menuitem == 2) {
    if (cycle == true) {
      cycle = false;
    }
    select.click = false;
    back.click = false;

    delay(500);
  }
  if (cycle && select.click && menuitem == 3) {
    select.click = false;
    startcycle();


  }
  //2ая страница
  if (page == 2 && down.click) {
    down.click = false;
    xArray++;
    if (xArray == n) {
      xArray = 0;
      page = 1;
    }
  }

} // void loop

void drawMenu() {

  if (page == 1) {
    display.setTextSize(1);
    display.clearDisplay();
    display.setTextColor(BLACK, WHITE);
    display.setCursor(15, 0);
    display.print("MAIN MENU");
    display.drawFastHLine(0, 10, 83, BLACK);
    display.setCursor(0, 15);

    if (menuitem == 1) {
      display.setTextColor(WHITE, BLACK);
    } else {
      display.setTextColor(BLACK, WHITE);
    }
    display.print(">Setup Drive");//Drivesetup
    display.setCursor(75, 15); // print Drive select
    display.print(selectdrive);

    display.setCursor(0, 25);

    if (menuitem == 2) {
      display.setTextColor(WHITE, BLACK);
    } else {
      display.setTextColor(BLACK, WHITE);
    }
    display.print(">Drive");

    display.setCursor(35, 25); // print Drive select
    display.print(" mm=");
    display.print(selectrepeat);
    if (menuitem == 3) {
      display.setTextColor(WHITE, BLACK);
    } else {
      display.setTextColor(BLACK, WHITE);
    }
    display.setCursor(0, 35);
    display.print(">Start ");
  }
  if (page == 2) {
    setupCycle();
  } //page 2
  //page 2
  display.display();
} //drawmenu


void setupCycle() {
  display.setTextSize(1);
  display.clearDisplay();
  display.setTextColor(BLACK, WHITE);
  display.setCursor(10, 0);
  display.print("SetupDrive");
  display.setCursor(75, 0);
  display.print(selectdrive);//selectdrive
  display.drawFastHLine(0, 10, 83, BLACK);
  display.setCursor(0, 15);

  switch (selectdrive) {
    case 0:
      for (int i = 0; i < n; i++) {
        asd[i] = asd1[i];

      }

      break;
    case 1:
      for (int i = 0; i < n; i++) {
        asd[i] = asd2[i];
      }
      break;

  }

  for (i = 0; i < n; i++) {
    display.print(asd[i]);
  }
  display.setCursor(0, 35);
  switch (xArray) {
    case 0:
      if (select.click) {
        asd[xArray] = 1;
        select.click = false;
      }
      if (asd[xArray] == 1 && back.click) {
        back.click = false;
        asd[xArray] = 0;
      }
      display.print(asd[xArray]);
      display.setCursor(35, 35);
      display.print("tact");
      display.setCursor(15, 35);
      display.print(xArray + 1);
      break;
    case 1:
      if (select.click) {
        asd[xArray] = 1;
        select.click = false;
      }
      if (asd[xArray] == 1 && back.click) {
        back.click = false;
        asd[xArray] = 0;
      }
      display.print(asd[xArray]);
      display.setCursor(35, 35);
      display.print("tact");
      display.setCursor(15, 35);
      display.print(xArray + 1);
      break;
    case 2:
      if (select.click) {
        asd[xArray] = 1;
        select.click = false;
      }
      if (asd[xArray] == 1 && back.click) {
        back.click = false;
        asd[xArray] = 0;
      }
      display.print(asd[xArray]);
      display.setCursor(35, 35);
      display.print("tact");
      display.setCursor(15, 35);
      display.print(xArray + 1);
      break;
    case 3:
      if (select.click) {
        asd[xArray] = 1;
        select.click = false;
      }
      if (asd[xArray] == 1 && back.click) {
        back.click = false;
        asd[xArray] = 0;
      }
      display.print(asd[xArray]);
      display.setCursor(35, 35);
      display.print("tact");
      display.setCursor(15, 35);
      display.print(xArray + 1);
      break;
    case 4:
      if (select.click) {
        asd[xArray] = 1;
        select.click = false;
      }
      if (asd[xArray] == 1 && back.click) {
        back.click = false;
        asd[xArray] = 0;
      }
      display.print(asd[xArray]);
      display.setCursor(35, 35);
      display.print("tact");
      display.setCursor(15, 35);
      display.print(xArray + 1);
      break;
    case 5:
      if (select.click) {
        asd[xArray] = 1;
        select.click = false;
      }
      if (asd[xArray] == 1 && back.click) {
        back.click = false;
        asd[xArray] = 0;
      }
      display.print(asd[xArray]);
      display.setCursor(35, 35);
      display.print("tact");
      display.setCursor(15, 35);
      display.print(xArray + 1);
      break;
    case 6:
      if (select.click) {
        asd[xArray] = 1;
        select.click = false;
      }
      if (asd[xArray] == 1 && back.click) {
        back.click = false;
        asd[xArray] = 0;
      }
      display.print(asd[xArray]);
      display.setCursor(35, 35);
      display.print("tact");
      display.setCursor(15, 35);
      display.print(xArray + 1);
      break;
    case 7:
      if (select.click) {
        asd[xArray] = 1;
        select.click = false;
      }
      if (asd[xArray] == 1 && back.click) {
        back.click = false;
        asd[xArray] = 0;
      }
      display.print(asd[xArray]);
      display.setCursor(35, 35);
      display.print("tact");
      display.setCursor(15, 35);
      display.print(xArray + 1);
      break;
    case 8:
      if (select.click) {
        asd[xArray] = 1;
        select.click = false;
      }
      if (asd[xArray] == 1 && back.click) {
        back.click = false;
        asd[xArray] = 0;
      }
      display.print(asd[xArray]);
      display.setCursor(35, 35);
      display.print("tact");
      display.setCursor(15, 35);
      display.print(xArray + 1);
      break;
    case 9:
      if (select.click) {
        asd[xArray] = 1;
        select.click = false;
      }
      if (asd[xArray] == 1 && back.click) {
        back.click = false;
        asd[xArray] = 0;
      }
      display.print(asd[xArray]);
      display.setCursor(35, 35);
      display.print("tact");
      display.setCursor(15, 35);
      display.print(xArray + 1);
      break;
    case 10:
      if (select.click) {
        asd[xArray] = 1;
        select.click = false;
      }
      if (asd[xArray] == 1 && back.click) {
        back.click = false;
        asd[xArray] = 0;
      }
      display.print(asd[xArray]);
      display.setCursor(35, 35);
      display.print("tact");
      display.setCursor(15, 35);
      display.print(xArray + 1);
      break;

    case 11:
      display.setCursor(25, 35);
      display.print("return");
      if (asd[xArray] == 1 && back.click) {
        back.click = false;
        page = 1;
        xArray = 0;
      }
      break;
  }
  switch (selectdrive) {
    case 0:
      for (int i = 0; i < n; i++) {
        asd1[i] = asd[i];

      }

      break;
    case 1:
      for (int i = 0; i < n; i++) {
        asd2[i] = asd[i];
      }
      break;
  }

}
void startcycle() // функция старт цикла
{
  if (cycle == false) return;
  Serial.println("start cycle");
  byte someValue = selectrepeat;
  char binaryTest[7] = {0};
  someValue += 128;
  itoa(someValue, binaryTest, 2);
  char* string = binaryTest + 1;
  Serial.println(strrev(string));
  char* readyByte = string;
 

  for (i = 0; i < n; i++) {
 for (int k = 0; k < 7; k++) {
   char someVal = readyByte[k];
//   Serial.println(atoi(someVal));
    if (int(someVal) == 49) {
      sendpin(k, HIGH);
      Serial.println("send high");
    } ;
    if (int(someVal) == 48) {
      sendpin(k, LOW);
      Serial.println("send low");
    };
     delay(50);
  };

    down.checkPress();
    select.checkPress();
    back.checkPress();
    if (down.click || select.click || back.click) {
      cycle = false;
      page = 1;
      return;
    }

    if (asd1[i] == 1) {
      digitalWrite(LED_1_PIN, HIGH);


    } else {
      digitalWrite(LED_1_PIN, LOW);

    }
    if (asd2[i] == 1) {
      digitalWrite(LED_2_PIN, HIGH);

    } else {
      digitalWrite(LED_2_PIN, LOW);

    }

    delay(2000);

  }

  return cycle = false;
}


// functions to send signal to register
void sendbyte(byte value) {
  if (Serial.available() > 0) {
    // read the incoming byte:

    // say what you got:
    Serial.print("I received: ");
    Serial.println("send to reg");


  };
  digitalWrite(latch, LOW);
  shiftOut(data, clock, LSBFIRST, value);
  digitalWrite(latch, HIGH);
}


void sendpin(int pin, boolean state) {
  pin--;
  states[pin] = state;

  byte value = 0;
  byte add = 1;
  for (int i = 0; i < 8; i++) {
    if (states[i] == HIGH) value += add;
    add *= 2;
  }
  digitalWrite(latch, LOW);
  shiftOut(data, clock, LSBFIRST, value);
  digitalWrite(latch, HIGH);
}

void cleanreg() {
  for (int i = 0; i < 8; i++) states[i] = LOW;
  digitalWrite(latch, LOW);
  shiftOut(data, clock, LSBFIRST, 0);
  digitalWrite(latch, HIGH);
}
